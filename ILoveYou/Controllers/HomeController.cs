﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ILoveYou.Models;
using ILoveYou.@base;

namespace ILoveYou.Controllers
{
    public class HomeController : Controller
    {
        private db21ef1e49474e4b85a59da21600b718c5Entities db = new db21ef1e49474e4b85a59da21600b718c5Entities();

        public ActionResult Index()
        {
            var text = db.Words.OrderBy(x => Guid.NewGuid()).First().Text;
            return View(new IndexModel(){Text = text});
        }

    }
}
